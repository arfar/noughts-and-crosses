use std::cmp::Ordering;
use std::fmt;

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum NoughtOrCrossOrEmpty {
    Nought,
    Cross,
    Empty,
}

pub struct GameState {
    pub first_player: NoughtOrCrossOrEmpty, // NOTE: will need to enforce that it can't be empty in code
    pub board: [[NoughtOrCrossOrEmpty; 3]; 3],
}

#[derive(Debug)]
pub enum MoveError {
    AlreadyOccupied(usize),
    TooBig(usize),
}

impl Default for GameState {
    fn default() -> GameState {
        GameState {
            first_player: NoughtOrCrossOrEmpty::Cross,
            board: [
                [
                    NoughtOrCrossOrEmpty::Empty,
                    NoughtOrCrossOrEmpty::Empty,
                    NoughtOrCrossOrEmpty::Empty,
                ],
                [
                    NoughtOrCrossOrEmpty::Empty,
                    NoughtOrCrossOrEmpty::Empty,
                    NoughtOrCrossOrEmpty::Empty,
                ],
                [
                    NoughtOrCrossOrEmpty::Empty,
                    NoughtOrCrossOrEmpty::Empty,
                    NoughtOrCrossOrEmpty::Empty,
                ],
            ],
        }
    }
}

impl fmt::Display for NoughtOrCrossOrEmpty {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            NoughtOrCrossOrEmpty::Cross => write!(f, "X"),
            NoughtOrCrossOrEmpty::Nought => write!(f, "O"),
            NoughtOrCrossOrEmpty::Empty => write!(f, " "),
        }
    }
}

impl GameState {
    pub fn winning_spots(&self) -> Option<[usize; 3]> {
        // This is the most basic way of doing this, but it should just werk
        // Diagonal Check
        if self.board[0][0] != NoughtOrCrossOrEmpty::Empty
            && self.board[0][0] == self.board[1][1]
            && self.board[1][1] == self.board[2][2]
        {
            return Some([0, 4, 8]);
        }
        // Other Diagonal Check
        if self.board[2][0] != NoughtOrCrossOrEmpty::Empty
            && self.board[2][0] == self.board[1][1]
            && self.board[1][1] == self.board[0][2]
        {
            return Some([2, 4, 6]);
        }
        // Horizontals
        for i in 0..3 {
            if self.board[i][0] != NoughtOrCrossOrEmpty::Empty
                && self.board[i][0] == self.board[i][1]
                && self.board[i][1] == self.board[i][2]
            {
                return Some([0 + i * 3, 1 + i * 3, 2 + i * 3]);
            }
        }
        // Verticals
        for i in 0..3 {
            if self.board[0][i] != NoughtOrCrossOrEmpty::Empty
                && self.board[0][i] == self.board[1][i]
                && self.board[1][i] == self.board[2][i]
            {
                return Some([0 + i, 3 + i, 6 + i]);
            }
        }
        None
    }
    pub fn next_player(&self) -> NoughtOrCrossOrEmpty {
        let mut num_crosses = 0;
        let mut num_noughts = 0;
        for i in 0..3 {
            for j in 0..3 {
                match self.board[i][j] {
                    NoughtOrCrossOrEmpty::Empty => (),
                    NoughtOrCrossOrEmpty::Cross => num_crosses += 1,
                    NoughtOrCrossOrEmpty::Nought => num_noughts += 1,
                }
            }
        }
        match num_crosses.cmp(&num_noughts) {
            Ordering::Equal => self.first_player,
            Ordering::Less => NoughtOrCrossOrEmpty::Cross,
            Ordering::Greater => NoughtOrCrossOrEmpty::Nought,
        }
    }
    pub fn add_move(&mut self, pos: usize) -> Result<(), MoveError> {
        if pos >= 9 {
            return Err(MoveError::TooBig(pos));
        }
        if self.board[pos / 3][pos % 3] != NoughtOrCrossOrEmpty::Empty {
            return Err(MoveError::AlreadyOccupied(pos));
        }
        self.board[pos / 3][pos % 3] = self.next_player();
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn new_game(first_player: NoughtOrCrossOrEmpty) -> GameState {
        assert!(first_player != NoughtOrCrossOrEmpty::Empty);
        GameState {
            first_player,
            ..Default::default()
        }
    }

    #[test]
    #[should_panic(expected = "assertion failed")]
    fn create_new_games() {
        let g = new_game(NoughtOrCrossOrEmpty::Cross);
        for i in 0..9 {
            println!("{}, {}: {:#?}", i / 3, i % 3, g.board[i / 3][i % 3]);
            assert_eq!(g.board[i / 3][i % 3], NoughtOrCrossOrEmpty::Empty);
        }
        // Should panic - keep this last
        let _ = new_game(NoughtOrCrossOrEmpty::Empty);
    }

    #[test]
    fn add_first_move() {
        let mut g = new_game(NoughtOrCrossOrEmpty::Nought);
        assert!(g.add_move(0).is_ok());
        assert_eq!(g.board[0][0], NoughtOrCrossOrEmpty::Nought);
        assert!(g.add_move(10).is_err());
    }

    #[test]
    fn add_conflicting_move() {
        let mut g = new_game(NoughtOrCrossOrEmpty::Nought);
        assert!(g.add_move(0).is_ok());
        let res = g.add_move(0);
        assert!(res.is_err());
    }

    #[test]
    fn check_non_win_states() {
        let mut g = new_game(NoughtOrCrossOrEmpty::Nought);
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(0).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(1).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(2).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(3).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(4).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(5).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(7).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(6).is_ok());
        assert!(g.winning_spots().is_none());
    }

    #[test]
    fn check_win_states() {
        // Zeroth Horizontal
        let mut g = new_game(NoughtOrCrossOrEmpty::Cross);
        assert!(g.add_move(0).is_ok());
        assert!(g.add_move(3).is_ok());
        assert!(g.add_move(1).is_ok());
        assert!(g.add_move(4).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(2).is_ok());
        assert!(g.winning_spots().is_some());
        assert_eq!(g.winning_spots(), Some([0, 1, 2]));
        // First Diagonal
        let mut g = new_game(NoughtOrCrossOrEmpty::Cross);
        assert!(g.add_move(0).is_ok());
        assert!(g.add_move(3).is_ok());
        assert!(g.add_move(4).is_ok());
        assert!(g.add_move(1).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(8).is_ok());
        assert!(g.winning_spots().is_some());
        assert_eq!(g.winning_spots(), Some([0, 4, 8]));

        // Other Diagonal
        let mut g = new_game(NoughtOrCrossOrEmpty::Cross);
        assert!(g.add_move(2).is_ok());
        assert!(g.add_move(3).is_ok());
        assert!(g.add_move(4).is_ok());
        assert!(g.add_move(1).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(6).is_ok());
        assert!(g.winning_spots().is_some());
        assert_eq!(g.winning_spots(), Some([2, 4, 6]));

        // Zeroth Vertical
        let mut g = new_game(NoughtOrCrossOrEmpty::Cross);
        assert!(g.add_move(0).is_ok());
        assert!(g.add_move(2).is_ok());
        assert!(g.add_move(3).is_ok());
        assert!(g.add_move(4).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(6).is_ok());
        assert!(g.winning_spots().is_some());
        // Oneth Vertical
        let mut g = new_game(NoughtOrCrossOrEmpty::Cross);
        assert!(g.add_move(1).is_ok());
        assert!(g.add_move(2).is_ok());
        assert!(g.add_move(4).is_ok());
        assert!(g.add_move(5).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(7).is_ok());
        assert!(g.winning_spots().is_some());
        // Twoth Vertical
        let mut g = new_game(NoughtOrCrossOrEmpty::Cross);
        assert!(g.add_move(2).is_ok());
        assert!(g.add_move(3).is_ok());
        assert!(g.add_move(5).is_ok());
        assert!(g.add_move(6).is_ok());
        assert!(g.winning_spots().is_none());
        assert!(g.add_move(8).is_ok());
        assert!(g.winning_spots().is_some());
    }
}
