use ratatui::{
    layout::{Constraint, Layout, Rect},
    style::{Color, Modifier, Style, Stylize},
    text::Line,
    widgets::{Block, Borders, Paragraph},
    Frame,
};

use crate::noughts::{GameState, NoughtOrCrossOrEmpty};

use std::rc::Rc;

pub fn ui(f: &mut Frame, app: &GameState) {
    let game_piece_dimension: u16 = 3;
    let game_padding_dimension_horizontal: u16 = 3;
    let game_padding_dimension_vertical: u16 = 1;
    let game_board_dimension_horizontal: u16 =
        game_piece_dimension * 3 + game_padding_dimension_horizontal * 2;
    let game_board_dimension_vertical: u16 =
        game_piece_dimension * 3 + game_padding_dimension_vertical * 2;

    let vertical = Layout::vertical([
        Constraint::Length(3),
        Constraint::Length(game_board_dimension_vertical),
        Constraint::Length(2),
        Constraint::Min(0),
    ]);
    let [opening_text_area, game_board_area, bottom_line, _] = vertical.areas(f.size());
    let status_line = format!(
        "It's {}'s turn - <0-8> to place - <q> to quit",
        app.next_player()
    );
    f.render_widget(
        Paragraph::new(vec![
            Line::from("Welcome to Noughts and cRoSses".magenta()).centered(),
            Line::from(status_line).centered(),
        ]),
        opening_text_area,
    );

    let padding_space = f.size().width - game_board_dimension_horizontal;
    let game_board_centered = Layout::horizontal([
        Constraint::Length(padding_space / 2),
        Constraint::Length(game_board_dimension_horizontal),
        Constraint::Length(padding_space / 2),
    ])
    .split(game_board_area);

    let board_rows = Layout::vertical([
        Constraint::Length(game_piece_dimension),
        Constraint::Length(game_padding_dimension_vertical),
        Constraint::Length(game_piece_dimension),
        Constraint::Length(game_padding_dimension_vertical),
        Constraint::Length(game_piece_dimension),
        Constraint::Min(0),
    ])
    .split(game_board_centered[1]);

    let mut rects = Vec::new();
    for board_row in board_rows.iter() {
        let row_cols = Layout::horizontal([
            Constraint::Length(game_piece_dimension),
            Constraint::Length(game_padding_dimension_horizontal),
            Constraint::Length(game_piece_dimension),
            Constraint::Length(game_padding_dimension_horizontal),
            Constraint::Length(game_piece_dimension),
            Constraint::Min(0),
        ])
        .split(*board_row);
        rects.push(row_cols)
    }

    draw_board_frame(f, &rects);

    let winning_spots = app.winning_spots();
    for (i, row) in app.board.into_iter().enumerate() {
        for (j, col) in row.into_iter().enumerate() {
            let index = i * 3 + j;
            if let Some(ws) = winning_spots {
                draw_piece(f, &rects, index, col, ws.contains(&index));
            } else {
                draw_piece(f, &rects, index, col, false);
            }
        }
    }
    if winning_spots.is_some() {
        f.render_widget(
            Paragraph::new(vec![
                Line::from(""),
                Line::from("The Game is Over - press <q> to quit".cyan()).centered(),
            ]),
            bottom_line,
        );
    }
}

fn draw_board_frame(f: &mut Frame, r: &[Rc<[Rect]>]) {
    for (row, item) in r.iter().enumerate().take(5) {
        if row % 2 == 1 {
            f.render_widget(Paragraph::new("═══"), item[0]);
            f.render_widget(Paragraph::new("═╬═"), item[1]);
            f.render_widget(Paragraph::new("═══"), item[2]);
            f.render_widget(Paragraph::new("═╬═"), item[3]);
            f.render_widget(Paragraph::new("═══"), item[4]);
        }
        if row % 2 == 0 {
            f.render_widget(Paragraph::new(" ║ \n ║ \n ║ "), item[1]);
            f.render_widget(Paragraph::new(" ║ \n ║ \n ║ "), item[3]);
        }
    }
}

fn draw_piece(
    f: &mut Frame,
    r: &[Rc<[Rect]>],
    index: usize,
    piece: NoughtOrCrossOrEmpty,
    bold: bool,
) {
    let row = (index / 3) * 2;
    let col = (index % 3) * 2;
    let style = if bold {
        Style::default().fg(Color::Red).add_modifier(Modifier::BOLD)
    } else {
        Style::default()
    };
    match piece {
        NoughtOrCrossOrEmpty::Cross => {
            f.render_widget(Paragraph::new("X X\n X \nX X").style(style), r[row][col])
        }
        NoughtOrCrossOrEmpty::Nought => {
            f.render_widget(Paragraph::new(" 0 \n0 0\n 0 ").style(style), r[row][col])
        }
        NoughtOrCrossOrEmpty::Empty => f.render_widget(
            Paragraph::new(format!("{}", index)).block(Block::new().borders(Borders::ALL)),
            r[row][col],
        ),
    }
}
