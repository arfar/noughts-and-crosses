#![allow(non_snake_case)]

use std::{error::Error, io};

use crossterm::{
    event::{self, Event, KeyCode, KeyEventKind},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};

mod noughts;
mod ui;

use noughts::GameState;
use ui::ui;

fn main() -> Result<(), Box<dyn Error>> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    let mut app = GameState::default();
    let _res = run_app(&mut terminal, &mut app);

    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
    terminal.show_cursor()?;

    Ok(())
}

fn run_app<B: Backend>(terminal: &mut Terminal<B>, app: &mut GameState) -> io::Result<bool> {
    loop {
        terminal.draw(|f| ui(f, app))?;
        if let Event::Key(key) = event::read()? {
            if key.kind == KeyEventKind::Release {
                // Skip events that are not KeyEventKind::Press
                continue;
            }
            let key_pressed = match key.code {
                KeyCode::Char('q') => return Ok(true),
                KeyCode::Char('0') => Some(0),
                KeyCode::Char('1') => Some(1),
                KeyCode::Char('2') => Some(2),
                KeyCode::Char('3') => Some(3),
                KeyCode::Char('4') => Some(4),
                KeyCode::Char('5') => Some(5),
                KeyCode::Char('6') => Some(6),
                KeyCode::Char('7') => Some(7),
                KeyCode::Char('8') => Some(8),
                KeyCode::Char('9') => Some(9),
                _ => None,
            };
            if let Some(i) = key_pressed {
                if app.winning_spots().is_some() {
                    continue;
                };
                let res = app.add_move(i);
                match res {
                    Ok(_) => continue,
                    // TODO display errors for these - or maybe I don't have to?
                    Err(noughts::MoveError::TooBig(_i)) => continue,
                    Err(noughts::MoveError::AlreadyOccupied(_i)) => continue,
                }
            }
        }
    }
}
