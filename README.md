# noughts and cRoSses

This is a very simple [noughts and crosses](https://en.wikipedia.org/wiki/Tic-tac-toe)
game written in rust. It uses [Ratatui](https://ratatui.rs) to display a simple little
game interface.

## Building

Should be as simple as ensuring you have an up to date rust compiler. This may work on
earlier versions of `rustc` or `ratatui`, however I haven't checked.

So, just try with:

`cargo run`

## Limitations / TODO

This does not do the following (but here are some features I thought I try add):
 - [ ] Allow different players to start
 - [ ] Allow some kind of scoring to be kept instead of forcing restart per-game
 - [ ] Add network play
 - [ ] Add a "[Connect Four](https://en.wikipedia.org/wiki/Connect_Four)" version
 
## License

I am _not_ a programmer and you should not be using this. Nevertheless, the code herein
is licensed under the GPLv3 or later - if you're silly enought to want to use this.
